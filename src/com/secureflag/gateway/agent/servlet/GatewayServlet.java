/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.secureflag.gateway.agent.actions.IAction;
import com.secureflag.gateway.agent.messages.MessageGenerator;
import com.secureflag.gateway.agent.model.Constants;

@SuppressWarnings("rawtypes")
public class GatewayServlet  extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected static org.slf4j.Logger logger = LoggerFactory.getLogger(GatewayServlet.class);

	protected static Map<String, Class> type2action = new HashMap<>();
	
	static {
		type2action.put(Constants.ACTION_GET_INSTANCE_STATUS, com.secureflag.gateway.agent.actions.GetInstanceStatusAction.class);
		type2action.put(Constants.ACTION_GET_RESULT_STATUS, com.secureflag.gateway.agent.actions.GetResultStatusAction.class);
		type2action.put(Constants.ACTION_GET_RESULT_ARCHIVE, com.secureflag.gateway.agent.actions.GetResultZipAction.class);

		type2action.put(Constants.ACTION_LIST_INSTANCE_IMAGES, com.secureflag.gateway.agent.actions.ListInstanceImagesAction.class);
		type2action.put(Constants.ACTION_PULL_INSTANCE_IMAGE, com.secureflag.gateway.agent.actions.PullInstanceImageAction.class);
		type2action.put(Constants.ACTION_REMOVE_INSTANCE_IMAGE, com.secureflag.gateway.agent.actions.RemoveInstanceImageAction.class);

	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		executeAction(request,response);
	}

	public void executeAction(HttpServletRequest request, HttpServletResponse response) {

		
		JsonObject jsonObject;
		String actionType;
		try {
			StringBuffer allLines = new StringBuffer();
			String output;
			while ((output = request.getReader().readLine()) != null) {
				allLines.append(output);
			}  
			jsonObject = (JsonObject) new JsonParser().parse(allLines.toString());
			JsonElement jActionType = jsonObject.get(Constants.JSON_ATTRIBUTE_ACTION);
			if (jActionType==null) {
				MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_TYPE_NOT_FOUND, response);
				return;
			}
			actionType = jActionType.getAsString();
		} catch (Exception e) {
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_JSON_PARSING, response);
			logger.error(e.getMessage());
			return;
		}

		Class actionClass = type2action.get(actionType);
		if (null == actionClass) {
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_NOT_FOUND, response);
			logger.error("Requested action "+actionType+" is not mapped");
			return;
		}

		try {
			IAction action = ((IAction) actionClass.newInstance());
			request.setAttribute(Constants.REQUEST_ATTRIBUTE_JSON, jsonObject);
			action.doAction(request,response);	
		} catch ( Exception  e) {
			logger.error(e.getMessage());
			MessageGenerator.sendErrorMessage(Constants.JSON_VALUE_ERROR_ACTION_EXCEPTION, response);
		}
	}
}