/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.model;

public class Constants {

	public static final String CONFIG_FILE = "config.properties";
	public static final String JSON_VALUE_ERROR = "error";
	public static final String JSON_VALUE_SUCCESS = "success";
	public static final String JSON_VALUE_REDIRECT = "redirect";
	public static final String JSON_ATTRIBUTE_RESULT = "result";
	public static final String JSON_ATTRIBUTE_LOCATION = "location";
	public static final String JSON_ATTRIBUTE_ERROR_MSG = "errorMsg";
	public static final String JSON_ATTRIBUTE_ACTION = "action";
	public static final String REQUEST_ATTRIBUTE_JSON = "json";
	
	public static final String ERROR_COULD_NOT_FETCH = "CouldNotFetch";
	
	public static final String JSON_VALUE_ERROR_FAILED_LOGIN = "Login failed";
	public static final String JSON_VALUE_ERROR_JSON_PARSING = "Json format not valid";
	public static final String JSON_VALUE_ERROR_ACTION_NOT_FOUND = "Action not found";
	public static final String JSON_VALUE_ERROR_ACTION_NOT_VALIDATED = "Action message not valid";
	public static final String JSON_VALUE_ERROR_ACTION_NOT_AUTHORIZED = "Action not authorised";
	public static final String JSON_VALUE_ERROR_ACTION_EXCEPTION = "Action exception";
	public static final String JSON_VALUE_ERROR_ACTION_INVALID_CSRF = "Invalid token";
	public static final String JSON_VALUE_ERROR_TYPE_NOT_FOUND = "Json Attribute '"+Constants.JSON_ATTRIBUTE_ACTION+"' missing";
	public static final String JSON_VALUE_ERROR_ACCOUNT_LOCKOUT = "Account lockout, please contact support.";
	public static final String JSON_VALUE_ERROR_INVALID_CONTENT_TYPE = "Invalid content type";

	public static final String REQUEST_JSON = "json";
	
	public static final String CONTENT_TYPE_HEADER = "Content-Type";
	public static final String CONTENT_TYPE_JSON = "application/json";

	public static final String ACTION_PARAM_ID = "id";
	public static final String INSTANCE_IP = "ip";

	public static final String ACTION_GET_RESULT_STATUS = "getResultStatus";
	public static final String ACTION_GET_INSTANCE_STATUS = "getInstanceStatus";
	public static final String ACTION_GET_RESULT_ARCHIVE = "getResultArchive";
	public static final String ACTION_GET_LOGS_ARCHIVE = "getLogsArchive";

	
	public static final String CONTENT_DISPOSITION_HEADER = "Content-Disposition";
	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String BASIC_AUTH_HEADER = "Basic";

	public static final String CHARSET = "UTF-8";
	
	public static final String AGENT_USER = "agentUser";
	public static final String AGENT_PASSWORD = "agentPassword";
	public static final String POST_METHOD = "POST";
	public static final String URL_LIST_INSTANCE_IMAGES = "/agent/listImages";
	public static final String URL_PULL_INSTANCE_IMAGE = "/agent/pullImage";
	public static final String URL_REMOVE_INSTANCE_IMAGE = "/agent/removeImage";
	
	public static final String ACTION_LIST_INSTANCE_IMAGES = "listImages";
	public static final String ACTION_PULL_INSTANCE_IMAGE = "pullImage";
	public static final String ACTION_REMOVE_INSTANCE_IMAGE = "removeImage";
	public static final String IMAGE_URL = "url";
	public static final String PARAM_NAME_CHECK = "check";
	public static final String UPLOADER_URL = "http://localhost:7200/agent/upload";
	public static final String BASENAME = "baseName";



}
