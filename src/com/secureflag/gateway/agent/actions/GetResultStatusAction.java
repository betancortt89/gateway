/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.actions;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.secureflag.gateway.agent.messages.MessageGenerator;
import com.secureflag.gateway.agent.model.Constants;
import com.secureflag.gateway.agent.utils.HTTPUtils;

public class GetResultStatusAction extends IAction{

	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JsonObject actionObject = new JsonObject();
		actionObject.addProperty(Constants.JSON_ATTRIBUTE_ACTION, Constants.ACTION_GET_RESULT_STATUS);
		
		JsonObject json = (JsonObject) request.getAttribute(Constants.REQUEST_JSON);
		JsonElement instanceIpElement = json.get(Constants.INSTANCE_IP);
		if(null==instanceIpElement){
			MessageGenerator.sendErrorMessage("Missing IP", response);
			logger.warn("Received null string when requesting status");
			return;
		}
		String check;
		if(null!=json.get(Constants.PARAM_NAME_CHECK))
			check = json.get(Constants.PARAM_NAME_CHECK).getAsString();
		else
			check = "flags";
		actionObject.addProperty(Constants.PARAM_NAME_CHECK, check);

		String responseString = HTTPUtils.sendPostRequest(instanceIpElement.getAsString(), actionObject.toString());
		
		if(null==responseString){
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
			logger.warn("Received null string when requesting live results from "+instanceIpElement.getAsString());
			return;
		}
		try {
			response.setContentType(Constants.CONTENT_TYPE_JSON);
			PrintWriter out = response.getWriter();
			out.print(responseString);
			out.flush();
		} catch (Exception e) {
			logger.error(e.getMessage());
			MessageGenerator.sendErrorMessage(Constants.ERROR_COULD_NOT_FETCH, response);
		}
	}
}