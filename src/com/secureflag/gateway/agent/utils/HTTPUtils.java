/** 
 * This file is part of the SecureFlag Platform.
 * Copyright (c) 2020 SecureFlag Limited.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.secureflag.gateway.agent.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.secureflag.gateway.agent.model.Constants;
import com.secureflag.gateway.agent.model.FileResponse;

public class HTTPUtils {

	protected static Logger logger = LoggerFactory.getLogger(HTTPUtils.class);

	public static String sendGet(String url) throws Exception {
		try {
			String USER_AGENT = "Mozilla/5.0 - RF";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		}catch(Exception e) {
			return null;
		}
	}
	public static String sendPost(String url, String body) {
		URL obj;
		StringBuffer response = new StringBuffer();
		try {
			obj = new URL(url);
			logger.debug("#Sending request to URL : " + obj.toString()+" for "+body);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod(Constants.POST_METHOD);
			con.setRequestProperty(Constants.CONTENT_TYPE_HEADER,Constants.CONTENT_TYPE_JSON ); 
			con.setReadTimeout(10000);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug("#Received response for "+body+" - Length: "+response.length());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
		return response.toString();
	}
	public static String sendPostRequest(String baseHost, String body) {
		URL obj;
		StringBuffer response = new StringBuffer();
		try {
			obj = new URL("http://"+baseHost+"/results/handler");
			logger.debug("#Sending request to URL : " + obj.toString()+" for "+body);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod(Constants.POST_METHOD);
			con.setRequestProperty(Constants.CONTENT_TYPE_HEADER,Constants.CONTENT_TYPE_JSON ); 
			con.setReadTimeout(30000);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug("#Received response for "+body+" - Length: "+response.length());
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
		return response.toString();
	}
	public static FileResponse sendGetFileArchiveRequest(String baseHost, String body) {
		URL obj;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		try {
			obj = new URL("http://"+baseHost+"/results/handler");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod(Constants.POST_METHOD);
			con.setRequestProperty(Constants.CONTENT_TYPE_HEADER,Constants.CONTENT_TYPE_JSON);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(body);
			wr.flush();
			wr.close();
			logger.debug("#Sending file results request to URL : " + obj.toString()+" for "+body);
			InputStream is = con.getInputStream();
			String disposition = con.getHeaderField(Constants.CONTENT_DISPOSITION_HEADER);
			String contentType = con.getContentType();
			int nRead;
			byte[] data = new byte[16384];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			buffer.flush();
			FileResponse fr = new FileResponse();
			fr.setContentDisposition(disposition);
			fr.setContentType(contentType);
			fr.setFileContent(buffer.toByteArray());
			logger.debug("#Received results file from " + obj.toString()+" for "+body+" - Length: "+buffer.size());
			return fr;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return null;
		}
	}
}